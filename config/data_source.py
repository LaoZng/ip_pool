# !/usr/bin/env python
# coding=utf-8
# 功能包：数据库源
# 作者：LaoZng

from pony.orm import Database

from config.configure import conf

mysql = conf['mysql']
db = Database()

db.bind(
    provider='mysql',
    host=mysql['host'],
    port=int(mysql['port']),
    user=mysql['username'],
    passwd=mysql['password'],
    db=mysql['database']
)
