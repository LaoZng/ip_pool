import datetime
import time

from pony.orm import *

from config.data_source import db


class Ip_Pool(db.Entity):
    ip = PrimaryKey(str, nullable=False)
    is_valid = Optional(int, default=1)
    use_count = Optional(int, nullable=True)
    source = Optional(str, nullable=True)
    create_time = Optional(int, default=int(time.time()))


class Cookie_Pool(db.Entity):
    id = PrimaryKey(str, nullable=False)
    cookie = Optional(LongStr, nullable=True)
    is_valid = Optional(int, default=1)
    source = Optional(str, nullable=True)
    update_time = Optional(int, default=int(time.time()))


db.generate_mapping()
