# !/usr/bin/env python
# coding=utf-8
# 功能包：数据库操作
# 作者：LaoZng


from sql.orm import *


@db_session
def insert_ip_pool(ip):
    if not query_ip_pool_by_ip(ip):
        Ip_Pool(ip=ip,create_time=int(time.time()))
        print('插入{}数据成功'.format(ip), '*' * 100)
    else:
        print('{}数据已入库'.format(ip), '*' * 100)


@db_session
def insert_cookie_pool(cookie):
    _id = cookie.split('cna=', 1)[1].split(';', 1)[0]
    if not query_cookie_pool_by_id(_id):
        Cookie_Pool(id=_id,
                    cookie=cookie,
                    source="1688")
        print('插入{}数据成功'.format(_id), '*' * 100)
    else:
        print('{}数据已入库'.format(_id), '*' * 100)


# 随机取一个IP
@db_session
def query_ip_pool():
    sql = "SELECT * FROM ip_pool where is_valid=1 ORDER BY RAND() LIMIT 1 "
    return Ip_Pool.select_by_sql(sql)


# 搜索IP用于检查是否可用
@db_session
def query_ip_pool_all():
    sql = "SELECT * FROM ip_pool where is_valid=1"
    return Ip_Pool.select_by_sql(sql)


# 根据IP查询
@db_session
def query_ip_pool_by_ip(ip):
    sql = "SELECT * FROM ip_pool where ip='{}'".format(ip)
    return Ip_Pool.select_by_sql(sql)


# 根据ID查询cookie
@db_session
def query_cookie_pool_by_id(_id):
    sql = "SELECT * FROM cookie_pool where id='{}'".format(_id)
    return Cookie_Pool.select_by_sql(sql)


# 随机取一个cookie
@db_session
def query_cookie_pool():
    sql = "SELECT * FROM cookie_pool where is_valid=1 ORDER BY RAND() LIMIT 1"
    return Cookie_Pool.select_by_sql(sql)


# 检查失效cookie并更新状态 超过3分钟标记为有效
@db_session
def query_Cookie_pool_all_by_time(row_time):
    sql = "SELECT * FROM cookie_pool where is_valid=0 and update_time < {}".format(row_time)
    return Cookie_Pool.select_by_sql(sql)


# 更新cookie存活状态 超过3分钟标记为有效
@db_session
def update_cookie_pool_according_time_by_id(id):
    sql = "update cookie_pool set is_valid=1 where id='{}'".format(id)
    db.execute(sql)


# 删除失效ip
@db_session
def delete_ip_pool_by_ip(ip):
    sql = "delete from ip_pool where ip='{}'".format(ip)
    db.execute(sql)
    print('删除{}数据成功'.format(ip), '*' * 100)
