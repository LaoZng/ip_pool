import threading

import requests
from core.source_url import get_url

from sql.config_info import insert_ip_pool

header = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'
}


def acquire_ip():
    url = get_url()
    print('获取ip地址:{}'.format(url))
    try:
        reponse = requests.get(url, headers=header, timeout=5)
        if reponse.status_code == 200:
            parse_html(reponse.text)
    except:
        print('请求ip异常:{}'.format(url))


def parse_html(html):
    html = html.replace('\'', '').replace('b', '').replace('<r/>', '').replace('\r', '')
    ips = html.split("\n")
    for ip in ips:
        ip = ip.strip()
        if 'false' in ip:
            print('您的套餐今日已到达上限')
            return
        elif '' == ip:
            return
        else:
            if '.' in ip:
                threading.Thread(target=insert_ip_pool, args=(ip,)).start()
