import random

urls = [
    #优质代理
    # r'http://ip.16yun.cn:817/myip/pl/4c5702a3-647f-4c4d-80cd-e984a7f9ecb4/?s=vpjavcswrq&u=adfly-2021',  # 放第三方代理api
    #动态短效
    r'http://ip.16yun.cn:817/myip/pl/cc144d08-ffdf-4561-aceb-43f7a89d9650/?s=wmlzsacprn&u=adfly-2021'
]


def get_url():
    return urls[random.randint(0, len(urls) - 1)]  # 随机返回urls


if __name__ == '__main__':
    print(get_url())
