import requests


def check_ip(ip):
    try:
        ip = {
            "https": "https://%s" % (ip),
            "http": "http://%s" % (ip),
        }
        #去除超时和延迟较高的IP timeout按需修改
        r = requests.get('http://current.ip.16yun.cn:802', proxies=ip, timeout=3)
        if r.status_code == 200:
            return True
        else:
            return False
    except:
        return False
