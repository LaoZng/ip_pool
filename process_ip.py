# !/usr/bin/env python
# coding=utf-8
# 功能包：启动入口
# 作者：LaoZng

import threading
import time
from core.IpAcquire import acquire_ip
from core.IpVerify import check_ip
from sql.config_info import *


def main():
    print('程序启动')
    try:
        threading.Thread(target=CheckIpMain).start()
        threading.Thread(target=InsetIpMain).start()
        threading.Thread(target=CheckCookieMain).start()
    except:
        main()


def InsetIpMain():
    print('更新线程启动！！！')
    while True:
        try:
            acquire_ip()
            time.sleep(60)  # 10S取一次
        except:
            print("更新时有异常...")
            time.sleep(2)


def CheckIpMain():
    # 比更新线程慢一步启动
    time.sleep(5)
    while True:
        try:
            print('测试IP线程执行！！！')
            ips = query_ip_pool_all()
            for p in ips:
                ip = p.ip
                # 代理IP时间超过X分钟的直接删除. 400
                if int(time.time()) - p.create_time > 250:
                    print('检测到ip：{} 超过最大时限 直接删除'.format(ip), '*' * 100)
                    delete_ip_pool_by_ip(ip)
                else:
                    # 未超过十分钟的检测失效后删除
                    if not check_ip(ip):
                        delete_ip_pool_by_ip(ip)
            # time.sleep(10)
        except Exception as e:
            print(e)
            time.sleep(5)


def CheckCookieMain():
    while True:
        try:
            print('更新cookie线程执行！！！')
            row_time = int(time.time()) - 60 * 5
            cookies = query_Cookie_pool_all_by_time(row_time)
            for cookie in cookies:
                update_cookie_pool_according_time_by_id(cookie.id)
                print('标记cookie：{}为可用状态'.format(cookie.id))

            time.sleep(60)
        except Exception as e:
            print(e)
            time.sleep(150)


if __name__ == '__main__':
    main()
